<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use WeconfModules\Chat\Http\Controllers\Central\ChatConversationController;
use WeconfModules\Chat\Http\Controllers\Central\ChatMessageController;

Route::group([
    'prefix' => 'dashboard',
    'as' => 'dashboard.',
    'middleware' => [
        'auth:sanctum',
        'verified',
    ],
], function () {
    Route::group([
        'prefix' => 'chat',
        'as' => 'chat.conversations.',
        'middleware' => ['role:god|administrator|customer', 'feature:mod_chats,customer']
    ], static function () {
        // Conversations
        Route::get('conversations', [ChatConversationController::class, 'index'])->name('index');
        Route::get('conversations/create', [ChatConversationController::class, 'create'])->name('create');
        Route::post('conversations', [ChatConversationController::class, 'store'])->name('store');
        Route::get('conversations/{id}/edit', [ChatConversationController::class, 'edit'])->name('edit');
        Route::put('conversations/{id}', [ChatConversationController::class, 'update'])->name('update');
        Route::delete('conversations/{id}', [ChatConversationController::class, 'destroy'])->name('destroy');
        Route::put('conversations/{id}/clear', [ChatConversationController::class, 'clear'])->name('clear');
        // Messages
        Route::get('conversations/{id}/messages', [ChatMessageController::class, 'index'])
            ->name('messages.index');
        Route::delete('conversations/messages/{id}', [ChatMessageController::class, 'destroy'])
            ->name('messages.destroy');
    });
});
