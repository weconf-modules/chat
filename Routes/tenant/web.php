<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\CheckTenantForMaintenanceMode;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomainOrSubdomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use WeconfModules\Chat\Http\Controllers\ChatController;

Route::group([
    'as' => 'tenant.chat.',
    'middleware' => [
        'tenant',
        InitializeTenancyByDomainOrSubdomain::class,
        PreventAccessFromCentralDomains::class,
        CheckTenantForMaintenanceMode::class,
    ],
], function () {
    Route::get('chat', [ChatController::class, 'index'])->name('index');
});
