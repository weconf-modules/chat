<?php

use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\CheckTenantForMaintenanceMode;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomainOrSubdomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use WeconfModules\Chat\Http\Controllers\ChatController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'as' => 'tenant.api.chat.',
    'prefix' => 'api',
    'middleware' => [
        'api',
        InitializeTenancyByDomainOrSubdomain::class,
        PreventAccessFromCentralDomains::class,
        CheckTenantForMaintenanceMode::class,
    ],
], function () {
    Route::middleware(['auth:tenant', 'verified'])->group(function () {
        Route::get('chats/{slug}', [ChatController::class, 'show'])->name('show');
        Route::put('chats/{slug}', [ChatController::class, 'update'])->name('update');
    });
});
