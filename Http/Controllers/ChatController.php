<?php

namespace WeconfModules\Chat\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;
use WeconfModules\Chat\Entities\ChatConversation;
use WeconfModules\Chat\Entities\ChatMessage;
use WeconfModules\Chat\Events\ChatMessageSent;
use WeconfModules\Core\Entities\User;

class ChatController extends Controller
{
    /**
     * TODO: deprecated?
     *
     * @return Response
     */
    public function index(): Response
    {
        /** @var ChatConversation $chat */
        $chat = ChatConversation::where('slug', '=', 'main')->first();

        if (!$chat) {
            abort(404);
        }

        $chatMessages = $chat->messages()::with('user')->paginate(45);

        return Inertia::render("modules:chat:Tenant/Chat", [
            'messages' => $chatMessages,
        ]);
    }

    /**
     * @param string $slug
     *
     * @return JsonResponse
     */
    public function show(string $slug): JsonResponse
    {
        /** @var ChatConversation $chat */
        $chat = ChatConversation::where('slug', $slug)->first();

        if (!$chat) {
            return response()->json(['message' => "Not found"]);
        }

        $chatMessages = $chat->messages()
            ->with('user')
            ->latest()
            ->paginate(500)
            ->toArray();

        // Sorts messages by creation date in ascending order.
        $chatMessages['data'] = array_values(Arr::sort($chatMessages['data'], static function ($_message) {
            return $_message['created_at'];
        }));

        return response()->json([
            'messages' => $chatMessages,
        ]);
    }

    /**
     * @param string $slug
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function update(string $slug, Request $request): JsonResponse
    {
        /** @var ChatConversation $chat */
        $chat = ChatConversation::where('slug', $slug)->first();

        if (!Auth::guard('tenant')->check()) {
            return response()->json(['message' => "Not authorized"]);
        }

        /** @var User $user */
        $user = Auth::guard('tenant')->user();

        /** @var ChatMessage $newMessage */
        $newMessage = $chat->messages()->create([
            'user_id' => $user->id,
            'text' => $request->input('text'),
        ]);

        $newMessage->load('user');

        if (App::environment() !== 'testing') {
            ChatMessageSent::dispatch([
                'channel' => $chat->channel_name,
                'message' => $newMessage->toArray(),
            ]);
        }

        return response()->json(['channel' => $chat->channel_name]);
    }
}
