<?php

namespace WeconfModules\Chat\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use LaravelJsonApi\Core\Responses\DataResponse;
use LaravelJsonApi\Laravel\Http\Controllers\Actions;
use Request;
use WeconfModules\Chat\Entities\ChatConversation;
use WeconfModules\Chat\JsonApi\V1\ChatConversations\ChatConversationSchema;

class ChatConversationController extends Controller
{

    use Actions\FetchMany;
    use Actions\FetchOne;
    use Actions\Store;
    use Actions\Update;
    use Actions\Destroy;
    use Actions\FetchRelated;
    use Actions\FetchRelationship;
    use Actions\UpdateRelationship;
    use Actions\AttachRelationship;
    use Actions\DetachRelationship;

}
