<?php

namespace WeconfModules\Chat\Http\Controllers\Central;

use App\Enums\Permission;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Inertia\Response;
use ProtoneMedia\LaravelQueryBuilderInertiaJs\InertiaTable;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use WeconfModules\Chat\Entities\ChatConversation;
use WeconfModules\Chat\Entities\ChatMessage;
use WeconfModules\Chat\Events\ChatMessageDelete;
use WeconfModules\Chat\Sorts\UserNameSort;

class ChatMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse|Response
     */
    public function index(Request $request, int $id): Response|RedirectResponse
    {
        abort_unless($request->user()->can(Permission::CHAT_MESSAGES), 401);

        $tenant = $request->user()->currentTenant;

        if (!$tenant) {
            return Redirect::back(303)->with('error', "Tenant not found");
        }

        $props = $tenant->run(function () use ($id) {
            $globalSearch = AllowedFilter::callback('global', static function ($query, $value) {
                $query->where(function ($query) use ($value) {
                    $query->where('text', 'ilike', "%{$value}%")
                        ->orWhereHas('user', function ($query) use ($value) {
                            $value = (string)Str::of($value)->studly()->lower();
                            $query->where(DB::raw('concat(first_name, middle_name, last_name)'), 'ilike', "%{$value}%");
                        });
                });
            }, null, "");

            $chatMessages = QueryBuilder::for(ChatMessage::class)
                ->where('chat_conversation_id', $id)
                ->with('user')
                ->defaultSort('-created_at')
                ->allowedSorts([
                    'text',
                    'created_at',
                    AllowedSort::custom("user_name", new UserNameSort()),
                ])
                ->allowedFilters($globalSearch)
                ->paginate(10)
                ->withQueryString()
                ->toArray();

            return [
                'entity' => ChatConversation::find($id)->toArray(),
                'messages' => $chatMessages,
            ];
        });

        return Inertia::render('modules:chat:Central/Views/ChatMessages/Index', $props)
            ->table(function (InertiaTable $table) {
            });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        abort_unless($request->user()->can(Permission::CHAT_MESSAGES_DELETE), 401);

        $request->user()->currentTenant->run(function () use ($id) {
            $message = ChatMessage::find($id);
            abort_unless($message, 404);
            $chat = ChatConversation::find($message->chat_conversation_id);
            $message->delete();

            if (App::environment() !== 'testing') {
                ChatMessageDelete::dispatch([
                    'channel' => $chat->channel_name,
                    'message' => [
                        'id' => $id,
                    ],
                ]);
            }
        });

        return back()->with('success', "Chat message deleted");
    }
}
