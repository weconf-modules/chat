<?php

namespace WeconfModules\Chat\Http\Controllers\Central;

use App\Enums\Permission;
use App\Models\Feature;
use App\Models\Tenant;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Inertia\Response;
use ProtoneMedia\LaravelQueryBuilderInertiaJs\InertiaTable;
use Spatie\QueryBuilder\QueryBuilder;
use WeconfModules\Chat\Entities\ChatConversation;

class ChatConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function index(Request $request): Response|RedirectResponse
    {
        /** @var Tenant $tenant */
        $tenant = $request->user()->currentTenant;

        if (!$tenant) {
            return Redirect::back(303)->with('error', "Tenant not found");
        }

        $entities = $tenant->run(function () {
            return QueryBuilder::for(ChatConversation::class)
                ->defaultSort('id')
                ->paginate(10)
                ->withQueryString()
                ->toArray();
        });

        return Inertia::render('modules:chat:Central/Views/ChatConversations/Index', [
            'entities' => $entities,
        ])->table(function (InertiaTable $table) {
            $table->disableGlobalSearch();
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    final public function create(Request $request): Response
    {
        abort_unless($request->user()->can(Permission::CHAT_CREATE), 401);

        return Inertia::render("modules:chat:Central/Views/ChatConversations/Create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        abort_unless($request->user()->can(Permission::CHAT_CREATE), 401);

        $validated = $request->validate([
            'name' => "required|string|max:255",
        ]);

        /** @var Tenant $tenant */
        $tenant = $request->user()->currentTenant;

        $tenant->run(function () use ($validated) {
            ChatConversation::create($validated);
        });

        $tenant->subscriptionUsageRecord(Feature::MOD_CHATS);

        return Redirect::route('dashboard.chat.conversations.index')
            ->with('success', "Chat conversation created");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, int $id): Response
    {
        abort_unless($request->user()->can(Permission::CHAT_UPDATE), 401);

        $entity = $request->user()->currentTenant->run(function () use ($id) {
            return ChatConversation::find($id)->toArray();
        });

        return Inertia::render('modules:chat:Central/Views/ChatConversations/Edit', [
            'entity' => $entity,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        abort_unless($request->user()->can(Permission::CHAT_UPDATE), 401);

        $validated = $request->validate([
            'name' => "required|string|max:255",
            'slug' => "nullable|string|max:255",
        ]);

        $request->user()->currentTenant->run(function () use ($id, $validated) {
            $validated['slug'] = empty($validated['slug'])
                ? Str::slug($validated['name'])
                : Str::slug($validated['slug']);

            $entity = ChatConversation::find($id);
            $entity->forceFill($validated)->save();
        });

        return Redirect::route('dashboard.chat.conversations.index')
            ->with('success', "Chat conversation updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        abort_unless($request->user()->can(Permission::CHAT_DELETE), 401);

        /** @var Tenant $tenant */
        $tenant = $request->user()->currentTenant;

        $tenant->run(function () use ($id) {
            ChatConversation::find($id)->delete();
        });

        $tenant->subscriptionUsageReduce(Feature::MOD_CHATS);

        return Redirect::route('dashboard.chat.conversations.index')
            ->with('success', "Chat conversation deleted");
    }

    /**
     * Clear chat from messages.
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function clear(Request $request, int $id): RedirectResponse
    {
        $request->user()->currentTenant->run(function () use ($request, $id) {
            $olderDateTime = Carbon::now()
                ->subDays((integer)$request->input('older_days'))
                ->toDateTimeString();

            ChatConversation::find($id)
                ->messages()
                ->where('created_at', '<=', $olderDateTime)
                ->delete();
        });

        return Redirect::route('dashboard.chat.conversations.messages.index', $id)
            ->with('success', "Chat cleared of messages");
    }
}
