const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
    "./vendor/laravel/jetstream/**/*.blade.php",
    "./storage/framework/views/*.php",
    "./resources/views/**/*.blade.php",
    "./resources/js/**/*.vue",
    "./modules/**/Resources/js/**/*.vue",
    "./local-modules/**/Resources/js/**/*.vue",
    "./node_modules/vue-tailwind/dist/*.js",
  ],
  theme: {
    borderRadius: {
      none: "0px",
      sm: "0.125rem",
      DEFAULT: "0.25rem",
      md: "0.375rem",
      lg: "0.5rem",
      xl: "0.75rem",
      "2xl": "1rem",
      "3xl": "1.5rem",
      full: "9999px",
    },
    extend: {
      fontFamily: {
        sans: ["Inter", ...defaultTheme.fontFamily.sans],
        verdana: ["Verdana", "Geneva", "sans-serif"],
      },
      colors: {
        /* text */
        "theme-text": "var(--text)",
        /* link */
        "theme-link": "var(--link)",
        /* backgrounds */
        "theme-background": "var(--background)",
        "theme-sidebar-background": "var(--sidebar-background)",
        "theme-topbar-background": "var(--topbar-background)",
        "theme-card-background": "var(--card-background)",
        "theme-chat-background": "var(--chat-background)",
        "theme-modal-background": "var(--modal-background)",
        /* primary */
        "theme-primary-lightest": "var(--primary-lightest)",
        "theme-primary-light": "var(--primary-light)",
        "theme-primary": "var(--primary)",
        "theme-primary-dark": "var(--primary-dark)",
        "theme-primary-darkest": "var(--primary-darkest)",
        /* secondary */
        "theme-secondary-lightest": "var(--secondary-lightest)",
        "theme-secondary-light": "var(--secondary-light)",
        "theme-secondary": "var(--secondary)",
        "theme-secondary-dark": "var(--secondary-dark)",
        "theme-secondary-darkest": "var(--secondary-darkest)",
        /* sidebar navigation */
        "theme-sidebar-navigation": "var(--sidebar-navigation)",
        "theme-sidebar-navigation-text": "var(--sidebar-navigation-text)",
        "theme-sidebar-navigation-active": "var(--sidebar-navigation-active)",
        "theme-sidebar-navigation-text-active":
          "var(--sidebar-navigation-text-active)",
        /* input */
        "theme-input-lightest": "var(--input-lightest)",
        "theme-input-light": "var(--input-light)",
        "theme-input": "var(--input)",
        "theme-input-dark": "var(--input-dark)",
        "theme-input-darkest": "var(--input-darkest)",
      },
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography"),
    require("@tailwindcss/aspect-ratio"),
  ],
};
