<?php

namespace WeconfModules\Chat\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;
use Stancl\VirtualColumn\VirtualColumn;
use WeconfModules\Chat\Database\Factories\ChatMessageFactory;
use WeconfModules\Core\Entities\User;

class ChatMessage extends Model
{
    use HasFactory;
    use VirtualColumn;

    protected $fillable = ['text', 'user_id'];

    public static function getCustomColumns(): array
    {
        return [
            'id',
            'user_id',
            'chat_conversation_id',
            'text',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @return ChatMessageFactory
     */
    protected static function newFactory(): ChatMessageFactory
    {
        return ChatMessageFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function conversation(): BelongsTo
    {
        return $this->belongsTo(ChatConversation::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
