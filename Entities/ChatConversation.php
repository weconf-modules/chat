<?php

namespace WeconfModules\Chat\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Stancl\VirtualColumn\VirtualColumn;
use WeconfModules\Chat\Database\Factories\ChatConversationFactory;

/**
 * Class ChatConversation
 *
 * @package WeconfModules\Chat\Entities
 * @property int    $id
 * @property string $name
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 * @property string $channel_name
 */
class ChatConversation extends Model
{
    use HasFactory;
    use HasSlug;
    use VirtualColumn;

    protected $fillable = ['name'];

    protected $appends = ["messages_number", "channel_name"];

    public static function getCustomColumns(): array
    {
        return [
            'id',
            'name',
            'slug',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(64)
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * @return ChatConversationFactory
     */
    protected static function newFactory(): ChatConversationFactory
    {
        return ChatConversationFactory::new();
    }

    /**
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(ChatMessage::class, 'chat_conversation_id');
    }

    /**
     * @return int
     */
    public function getMessagesNumberAttribute(): int
    {
        return $this->messages()->count();
    }

    /**
     * @return string|null
     */
    public function getChannelNameAttribute(): ?string
    {
        $tenant = tenant();

        return $tenant
            ? 'tenant_' . $tenant->id . '_chat_' . $this->slug
            : null;
    }
}
