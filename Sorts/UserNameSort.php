<?php

namespace WeconfModules\Chat\Sorts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\Sorts\Sort;

class UserNameSort implements Sort
{
    public function __invoke(Builder $query, bool $descending, string $property): void
    {
        $direction = $descending ? "DESC" : "ASC";

        $query
            ->select('chat_messages.*',
                DB::raw("concat(users.first_name, users.middle_name, users.last_name) AS {$property}"))
            ->join('users', static function ($join) {
                $join->on('chat_messages.user_id', '=', 'users.id');
            })
            ->orderBy($property, $direction);
    }
}
