const path = require("path");

module.exports = {
  module: {
    rules: [
      {
        test: /\.(postcss)$/,
        use: [
          "vue-style-loader",
          { loader: "css-loader", options: { importLoaders: 1 } },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [require("postcss-import"), require("tailwindcss")],
              },
            },
          },
        ],
      },
    ],
  },
};
