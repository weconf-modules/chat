<?php

namespace WeconfModules\Chat\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatMessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private array $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function broadcastAs(): string
    {
        return "message_sent";
    }

    /**
     * @return string[]
     */
    public function broadcastWith(): array
    {
        return $this->data['message'];
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): Channel
    {
        return new PrivateChannel($this->data['channel']);
    }
}
