<?php

namespace WeconfModules\Chat\Tests\Feature;

use App\Enums\Role;

class TenantChatCommonTest extends TenantChatTestCase
{
    public function test_chats_index_screen_can_be_rendered(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();

        $this->assertGuest()
            ->get(route('dashboard.chat.conversations.index'))
            ->assertRedirect(route('login'));

        $this->actingAs($this->user)
            ->get(route('dashboard.chat.conversations.index'))
            ->assertOk()
            ->assertInertia(fn($assert) => $assert
                ->component('modules:chat:Central/Views/ChatConversations/Index', false)
                ->has('entities')
            );
    }

    public function test_chats_index_screen_cannot_be_rendered_for_user_with_subscriber_role(): void
    {
        $this->user->assign(Role::SUBSCRIBER);

        $this->actingAs($this->user)
            ->get(route('dashboard.chat.conversations.index'))
            ->assertStatus(401);
    }

    public function test_create_chat_screen_can_be_rendered(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();

        $this->assertGuest()
            ->get(route('dashboard.chat.conversations.create'))
            ->assertRedirect(route('login'));

        $this->actingAs($this->user)
            ->get(route('dashboard.chat.conversations.create'))
            ->assertOk()
            ->assertInertia(fn($assert) => $assert
                ->component('modules:chat:Central/Views/ChatConversations/Create', false)
            );
    }

    public function test_create_chat_screen_cannot_be_rendered_for_user_with_subscriber_role(): void
    {
        $this->user->assign(Role::SUBSCRIBER);

        $this->actingAs($this->user)
            ->get(route('dashboard.chat.conversations.create'))
            ->assertStatus(401);
    }

    public function test_edit_chat_screen_can_be_rendered(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();
        $chat = $this->createChat();

        $this->assertGuest()
            ->get(route('dashboard.chat.conversations.edit', $chat->id))
            ->assertRedirect(route('login'));

        $this->actingAs($this->user)
            ->get(route('dashboard.chat.conversations.edit', $chat->id))
            ->assertOk()
            ->assertInertia(fn($assert) => $assert
                ->component('modules:chat:Central/Views/ChatConversations/Edit', false)
                ->has('entity')
                ->where('entity.id', $chat->id)
                ->where('entity.name', $chat->name)
            );
    }
}
