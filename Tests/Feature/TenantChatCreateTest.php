<?php

namespace WeconfModules\Chat\Tests\Feature;

use App\Enums\Role;

class TenantChatCreateTest extends TenantChatTestCase
{
    public function test_chat_can_be_created(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();

        // Create
        $chatData = $this->getChatData();
        $this->actingAs($this->user)
            ->post(route('dashboard.chat.conversations.store', $chatData))
            ->assertSessionHasNoErrors()
            ->assertRedirect(route('dashboard.chat.conversations.index'));

        // Check
        $chat = $this->getChat();
        $this->assertNotNull($chat, "The chat must be created.");
        $this->assertEquals($chatData['name'], $chat->name,
            "The chat name should match the default name.");
    }

    public function test_chat_cannot_be_created_for_user_with_subscriber_role(): void
    {
        $this->user->assign(Role::SUBSCRIBER);

        $this->actingAs($this->user)
            ->post(route('dashboard.chat.conversations.store', $this->getChatData()))
            ->assertStatus(401);
    }

    public function test_chat_cannot_be_created_with_empty_name(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();
        $chatData = $this->getChatData([
            'name' => "",
        ]);

        $this->actingAs($this->user)
            ->post(route('dashboard.chat.conversations.store', $chatData))
            ->assertSessionHasErrors('name')
            ->assertRedirect();
    }
}
