<?php

namespace WeconfModules\Chat\Tests\Feature;

use App\Enums\Role;

class TenantChatUpdateTest extends TenantChatTestCase
{
    public function test_chat_can_be_updated(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();

        // Update
        $chat = $this->createChat();
        $chatData = $this->getChatData([
            'name' => "Test chat 42",
        ]);

        $this->actingAs($this->user)
            ->put(route('dashboard.chat.conversations.update', $chat->id), $chatData)
            ->assertSessionHasNoErrors()
            ->assertRedirect(route('dashboard.chat.conversations.index'));

        // Check
        $chat = $this->getChat();
        $this->assertEquals($chatData['name'], $chat->name, "The chat name should match the updated name.");
    }

    public function test_chat_cannot_be_updated_for_user_with_subscriber_role(): void
    {
        $this->user->assign(Role::SUBSCRIBER);

        $this->actingAs($this->user)
            ->put(route('dashboard.chat.conversations.update', 1), $this->getChatData())
            ->assertStatus(401);
    }

    public function test_chat_cannot_be_updated_with_empty_name(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();
        $chat = $this->createChat();
        $chatData = $this->getChatData([
            'name' => "",
        ]);

        $this->actingAs($this->user)
            ->put(route('dashboard.chat.conversations.update', $chat->id), $chatData)
            ->assertSessionHasErrors('name')
            ->assertRedirect();
    }
}
