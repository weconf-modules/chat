<?php

namespace WeconfModules\Chat\Tests\Feature;

use Tests\CentralWithTenantTestCase;
use Tests\ToSubdomain;
use WeconfModules\Chat\Entities\ChatConversation;
use WeconfModules\Core\Entities\User;
use WeconfModules\Core\Entities\User as TenantUser;

class TenantSubdomainChatMessageTest extends CentralWithTenantTestCase
{
    use ToSubdomain;

    protected bool $tenantMustBeCreated = true;
    protected bool $tenantMustBeInitialized = true;

    protected User $user;
    protected string $domain = "first";

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->tenant->domains()->create([
            'domain' => $this->domain,
        ]);
    }

    public function test_chat_message_can_be_created(): void
    {
        $this->tenant->run(function () {
            $chat = ChatConversation::factory()->create();

            // Create
            $response = $this->actingAs($this->user, "tenant")
                ->put($this->toSubdomain(route('tenant.api.chat.update', $chat->slug)), [
                    'text' => "Test message",
                ])
                ->assertOk();

            // Check
            $chat->load('messages');
            $message = $chat->messages()->first();
            $channelName = $response->json('channel');
            $this->assertNotNull($channelName,
                "The `channel` key should be returned in the json response.");
            $this->assertEquals($channelName, $chat->channel_name,
                "The returned chat channel name must match the actual channel name.");
            $this->assertNotNull($message, "The chat message must be created.");
            $this->assertEquals("Test message", $message->text,
                "The message text should match the specified text.");
        });
    }

    public function test_chat_message_cannot_be_created_for_unauthorized_user(): void
    {
        $this->tenant->run(function () {
            $chat = ChatConversation::factory()->create();

            // Create
            $this->assertGuest()
                ->put($this->toSubdomain(route('tenant.api.chat.update', $chat->slug)), [
                    'text' => "Test message",
                ])
                ->assertStatus(302);

            // Check
            $chat->load('messages');
            $message = $chat->messages()->first();
            $this->assertNull($message, "The chat message must not be created.");
        });
    }

    public function test_chat_messages_can_be_received(): void
    {
        $this->tenant->run(function () {
            $chat = ChatConversation::factory()->create();
            $tenantUser = TenantUser::factory()->create();
            $chat->messages()->create([
                'user_id' => $tenantUser->id,
                'text' => "Test message",
            ]);

            $response = $this->actingAs($this->user, "tenant")
                ->get($this->toSubdomain(route('tenant.api.chat.show', $chat->slug)))
                ->assertOk();

            $messages = $response->json('messages');
            $this->assertNotNull($messages,
                "The `messages` key should be returned in the json response.");
            $message = array_shift($messages['data']);
            $this->assertNotNull($message, "The chat message should be received.");
            $this->assertEquals("Test message", $message['text'],
                "The chat message text should match the actual message text.");
        });
    }
}
