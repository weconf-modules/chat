<?php

namespace WeconfModules\Chat\Tests\Feature;

use App\Enums\Role;

class TenantChatMessageTest extends TenantChatTestCase
{
    public function test_chat_messages_index_screen_can_be_rendered(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();
        $chat = $this->createChat();
        $message = $this->createMessage($chat);

        $this->assertGuest()
            ->get(route('dashboard.chat.conversations.messages.index', $chat->id))
            ->assertRedirect(route('login'));

        $this->actingAs($this->user)
            ->get(route('dashboard.chat.conversations.messages.index', $chat->id))
            ->assertOk()
            ->assertInertia(fn($assert) => $assert
                ->component('modules:chat:Central/Views/ChatMessages/Index', false)
                ->hasAll([
                    'entity',
                    'messages.data' => 1,
                ])
                ->where('messages.data.0.id', $message->id)
                ->where('messages.data.0.text', $message->text)
            );
    }

    public function test_chat_messages_index_screen_cannot_be_rendered_for_user_with_subscriber_role(): void
    {
        $this->user->assign(Role::SUBSCRIBER);

        $this->actingAs($this->user)
            ->get(route('dashboard.chat.conversations.messages.index', 1))
            ->assertStatus(401);
    }

    public function test_chat_message_can_be_removed(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();
        $chat = $this->createChat();
        $message = $this->createMessage($chat);

        $this->actingAs($this->user)
            ->delete(route('dashboard.chat.conversations.messages.destroy', $message->id))
            ->assertRedirect();

        $message = $this->getMessage($chat);
        $this->assertNull($message, "The chat message must be removed.");
    }
}
