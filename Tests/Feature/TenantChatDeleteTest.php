<?php

namespace WeconfModules\Chat\Tests\Feature;

use App\Enums\Role;

class TenantChatDeleteTest extends TenantChatTestCase
{
    public function test_chat_can_be_removed(): void
    {
        $this->user->assign(Role::CUSTOMER);
        $this->setTenantSubscription();
        $chat = $this->createChat();

        $this->actingAs($this->user)
            ->delete(route('dashboard.chat.conversations.destroy', $chat->id))
            ->assertRedirect(route('dashboard.chat.conversations.index'));

        $chat = $this->getChat();
        $this->assertNull($chat, "The chat must be removed.");
    }

    public function test_chat_cannot_be_removed_for_user_with_subscriber_role(): void
    {
        $this->user->assign(Role::SUBSCRIBER);
        $chat = $this->createChat();

        $this->actingAs($this->user)
            ->delete(route('dashboard.chat.conversations.destroy', $chat->id))
            ->assertStatus(401);
    }
}
