<?php

namespace WeconfModules\Chat\Tests\Feature;

use App\Models\Plan;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use JetBrains\PhpStorm\Pure;
use Tests\CentralWithTenantTestCase;
use WeconfModules\Chat\Entities\ChatConversation;
use WeconfModules\Chat\Entities\ChatMessage;
use WeconfModules\Core\Entities\User as TenantUser;

class TenantChatTestCase extends CentralWithTenantTestCase
{
    protected bool $tenantMustBeCreated = true;
    protected User $user;
    private array $defaultChatData;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->withPersonalTeam()->create();
        $currentTeam = $this->user->currentTeam()->first();
        $currentTeam->tenants()->attach($this->tenant);
        $this->user->switchTenant($this->tenant);

        $this->defaultChatData = [
            'name' => "Test chat",
        ];
    }

    /**
     * @param string $planCode
     */
    protected function setTenantSubscription(string $planCode = Plan::TRIAL): void
    {
        $plan = Plan::code($planCode)->first();

        if ($plan) {
            $this->tenant->newSubscription($plan)->create();
        }
    }

    /**
     * @param array $customData
     *
     * @return array
     */
    #[Pure] protected function getChatData(array $customData = []): array
    {
        return array_merge($this->defaultChatData, $customData);
    }

    /**
     * @param array $customChatData
     *
     * @return ChatConversation
     */
    protected function createChat(array $customChatData = []): ChatConversation
    {
        return $this->tenant->run(function () use ($customChatData) {
            $data = array_merge($this->defaultChatData, $customChatData);
            return ChatConversation::factory()->create($data);
        });
    }

    /**
     * @param bool $asArray
     *
     * @return ChatConversation|array|null
     */
    protected function getChat(bool $asArray = false): ChatConversation|array|null
    {
        return $this->tenant->run(function () use ($asArray) {
            $chat = ChatConversation::first();

            return ($chat && $asArray)
                ? $chat->toArray()
                : $chat;
        });
    }

    /**
     * @param ChatConversation $chat
     *
     * @return ChatMessage
     */
    protected function createMessage(ChatConversation $chat): ChatMessage
    {
        return $this->tenant->run(function () use ($chat) {
            $user = TenantUser::factory()->create();
            return $chat->messages()->create([
                'user_id' => $user->id,
                'text' => "Test message",
            ]);
        });
    }

    /**
     * @param ChatConversation $chat
     *
     * @return ChatMessage|null
     */
    protected function getMessage(ChatConversation $chat): ?ChatMessage
    {
        return $this->tenant->run(function () use ($chat) {
            return $chat->messages()->first();
        });
    }
}
