const mix = require("laravel-mix");
mix
  .setPublicPath("dist")
  .js("Resources/assets/js/app.js", "js")
  .vue()
  .webpackConfig(require("./webpack.config"));
