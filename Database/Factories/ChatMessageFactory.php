<?php

namespace WeconfModules\Chat\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use WeconfModules\Chat\Entities\ChatMessage;

class ChatMessageFactory extends Factory
{
    /**
     * @var string
     */
    protected $model = ChatMessage::class;

    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id' => 1,
            'chat_conversation_id' => 1,
            'text' => $this->faker->text(),
        ];
    }
}
