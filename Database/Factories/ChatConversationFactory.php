<?php

namespace WeconfModules\Chat\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use WeconfModules\Chat\Entities\ChatConversation;

class ChatConversationFactory extends Factory
{
    /**
     * @var string
     */
    protected $model = ChatConversation::class;

    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
        ];
    }
}
